//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *bReset;
	TButton *Button2;
	TLayout *Layout1;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TLabel *lTrue;
	TLabel *lFalse;
	TLabel *lEquation;
	TButton *bYes;
	TButton *bNo;
	TLabel *Label4;
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall bResetClick(TObject *Sender);
	void __fastcall bYesClick(TObject *Sender);
	void __fastcall bNoClick(TObject *Sender);
private:	// User declarations
	int TrueCounter;
	int FalseCounter;
	bool CorrectAnswer;
	void doReset();
	void doContinue();
	void doAnswer(bool _CorrectAnswer);
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
