//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    ShowMessage("Made by M. Alexeev");
}


//---------------------------------------------------------------------------
void TForm1::doReset()
{
	TrueCounter = 0;
	FalseCounter = 0;
	doContinue();
}

void __fastcall TForm1::bResetClick(TObject *Sender)
{
	doReset();
}

void TForm1::doContinue()
{
	lTrue->Text = Format(L"True %d", ARRAYOFCONST((TrueCounter)));
	lFalse->Text = Format(L"False %d", ARRAYOFCONST((FalseCounter)));

	int value1 = random(20);
	int value2 = random(20);
	int sign = random(2) == 1 ? 1 : -1;
	int result = value1 + value2;
	int result_new = random(2) == 1 ? result : result + (random(7) + 1) * sign;

	CorrectAnswer = result == result_new;
	lEquation->Text = Format("%d + %d = %d", ARRAYOFCONST((value1, value2, result_new)));
}

void TForm1::doAnswer(bool _value)
{
	if (_value == CorrectAnswer)
	{
		TrueCounter++;
	}
	else
	{
		FalseCounter++;
    }
	doContinue();
}


//---------------------------------------------------------------------------

void __fastcall TForm1::bYesClick(TObject *Sender)
{
	doAnswer(True);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::bNoClick(TObject *Sender)
{
    doAnswer(False);
}
//---------------------------------------------------------------------------

