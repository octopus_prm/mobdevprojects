//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
    ShowMessage("Made by Alexeev Michael");
}
//---------------------------------------------------------------------------



void __fastcall TForm1::bPasClick(TObject *Sender)
{
	ePas->Text = RandomString(StrToIntDef(eLength->Text, 9),
		cbSS->IsChecked, cbNum->IsChecked, cbLower->IsChecked, cbUpper->IsChecked);
	eLength->Text = ePas->Text.Length();
}
//---------------------------------------------------------------------------

