//---------------------------------------------------------------------------

#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

System::UnicodeString RandomString(int eLength, bool cbSS, bool cbNum, bool cbLower, bool cbUpper)
{
	const char *SYM_CASE = "abcdefghijklmnopqrstuvwxyz";
	const char *SYM_NUMBER = "0123456789";
	const char *SYM_SPEC = "[]{},.:;-+=#!@#%&*()";

	System::UnicodeString vResult = "";
	System::UnicodeString vSym = "";

	if (cbSS) vSym += SYM_SPEC;
	if (cbNum) vSym += SYM_NUMBER;
	if (cbLower) vSym += SYM_CASE;
	if (cbUpper) vSym += UpperCase(SYM_CASE);

	if (vSym == "") vSym += SYM_CASE;

	while (vResult.Length() < eLength) {
		vResult = vResult + vSym[Random(vSym.Length())+1];
	}

	return vResult;
}
