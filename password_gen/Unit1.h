//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TButton *Button1;
	TLabel *Label1;
	TToolBar *ToolBar1;
	TEdit *ePas;
	TButton *bPas;
	TCheckBox *cbSS;
	TCheckBox *cbNum;
	TCheckBox *cbLower;
	TCheckBox *cbUpper;
	TLabel *Label2;
	TEdit *eLength;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall bPasClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
