//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	ProgressBar1->Max = tc1->TabCount-2;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::bStartClick(TObject *Sender)
{
    Memo1->Lines->Clear();
    tc1->Next();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::tc1Change(TObject *Sender)
{
	ProgressBar1->Value = tc1->ActiveTab->Index;	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::b1Click(TObject *Sender)
{
	if ( ((TButton*)Sender)->Tag == 1 ) {
		Memo1->Lines->Add("1. Yep");
	}
	else
	{
		Memo1->Lines->Add("1. Nope");
	}
	tc1->Next();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::b5Click(TObject *Sender)
{
	if ( ((TButton*)Sender)->Tag == 1 ) {
		Memo1->Lines->Add("2. Yep");
	}
	else
	{
		Memo1->Lines->Add("2. Nope");
	}
	tc1->Next();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::b9Click(TObject *Sender)
{
	if ( ((TButton*)Sender)->Tag == 1 ) {
		Memo1->Lines->Add("3. Yep");
	}
	else
	{
		Memo1->Lines->Add("3. Nope");
	}
	tc1->Next();	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Memo1->Lines->Clear();
	tc1->First();	
}
//---------------------------------------------------------------------------
