//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLabel *Label1;
	TTabControl *tc1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TProgressBar *ProgressBar1;
	TTabItem *TabItem5;
	TLabel *Label2;
	TButton *bStart;
	TGridPanelLayout *GridPanelLayout1;
	TLabel *Label3;
	TButton *b1;
	TButton *b2;
	TMemo *Memo1;
	TButton *b3;
	TButton *b4;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *Label4;
	TButton *b5;
	TButton *b6;
	TButton *b7;
	TButton *b8;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *Label5;
	TButton *b9;
	TButton *b10;
	TButton *b11;
	TButton *b12;
	TButton *Button1;
	void __fastcall bStartClick(TObject *Sender);
	void __fastcall tc1Change(TObject *Sender);
	void __fastcall b1Click(TObject *Sender);
	void __fastcall b5Click(TObject *Sender);
	void __fastcall b9Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
