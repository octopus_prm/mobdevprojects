//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include <System.UIConsts.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
#pragma resource ("*.SmXhdpiPh.fmx", _PLAT_ANDROID)
#pragma resource ("*.LgXhdpiPh.fmx", _PLAT_ANDROID)

Tfm1 *fm1;
//---------------------------------------------------------------------------
__fastcall Tfm1::Tfm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::Button3Click(TObject *Sender)
{
    ShowMessage("Made by M. Alexeev");
}

void Tfm1::NewColor(TAlphaColor _value)
{
	lValue->Text = AlphaColorToString(_value);
	lRGB->Text = Format("(%d, %d, %d)",
		ARRAYOFCONST ((
			TAlphaColorRec(_value).R,
			TAlphaColorRec(_value).G,
			TAlphaColorRec(_value).B
		))
	);

	fm1->Fill->Color = _value;
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::bRandomClick(TObject *Sender)
{
	NewColor(TAlphaColorF::Create(Random(256), Random(256), Random(256)).ToAlphaColor());
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::bCyanClick(TObject *Sender)
{
    NewColor(TAlphaColor(TAlphaColorRec::Cyan));
}
//---------------------------------------------------------------------------
void __fastcall Tfm1::FormCreate(TObject *Sender)
{
    bRandomClick(bRandom);
}
//---------------------------------------------------------------------------

