//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TFlowLayout *FlowLayout1;
	TButton *bRandom;
	TButton *bCyan;
	TButton *Button3;
	TLabel *lValue;
	TLabel *lRGB;
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall bRandomClick(TObject *Sender);
	void __fastcall bCyanClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
    void NewColor(TAlphaColor _value);
public:		// User declarations
	__fastcall Tfm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm1 *fm1;
//---------------------------------------------------------------------------
#endif
