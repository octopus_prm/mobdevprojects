//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.MultiView.hpp>
#include <FMX.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *tmain;
	TTabItem *tdiff;
	TTabItem *trules;
	TLayout *Layout1;
	TButton *Play;
	TButton *Rules;
	TButton *About;
	TButton *Exit;
	TToolBar *ToolBar1;
	TButton *Button1;
	TLabel *lDiff;
	TGridLayout *GridLayout1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TToolBar *ToolBar2;
	TButton *Button6;
	TLabel *lRules;
	void __fastcall PlayClick(TObject *Sender);
	void __fastcall RulesClick(TObject *Sender);
	void __fastcall ExitClick(TObject *Sender);
	void __fastcall AboutClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
