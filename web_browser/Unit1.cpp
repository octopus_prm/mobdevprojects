//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
    ShowMessage("Made by Alexeev Michael");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FindClick(TObject *Sender)
{
    WebBrowser1->URL = URL->Text;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::RefreshClick(TObject *Sender)
{
    WebBrowser1->Reload();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::StopClick(TObject *Sender)
{
    WebBrowser1->Stop();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	WebBrowser1->GoBack();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    WebBrowser1->GoForward();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::WebBrowser1DidFinishLoad(TObject *ASender)
{
    URL->Text = WebBrowser1->URL;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::URLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	   if (Key == vkReturn) {
			//WebBrowser1->URL = URL->Text;
			WebBrowser1->Navigate("https://www.google.co.kr/search?q=" + URL->Text);
	   }
}
//---------------------------------------------------------------------------
