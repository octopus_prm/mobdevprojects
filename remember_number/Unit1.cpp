//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "time.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}

void TForm1::doReset()
{
	cRight = 0;
	cWrong = 0;
	doContinue();
}

void TForm1::doContinue()
{
	lRigth->Text = "Right " + IntToStr(cRight);
	lWrong->Text = "Wrong " + IntToStr(cWrong);
	rNum = RandomRange(100000, 999999);
	lNum->Text = IntToStr(rNum);
	eType->Text = "";
	pb1->Value = 0;
    doTypeVisible(true);
}

void TForm1::doAnswer()
{
	int x = StrToInt(eType->Text);
	if (x == rNum) {
		cRight++;
	}
	else
	{
		cWrong++;
	}
	doContinue();
}

void TForm1::doTypeVisible(bool _value)
{
	Timer1->Enabled = _value;
	Label3->Visible = _value;
	RoundRect1->Visible = _value;
	pb1->Visible = _value;
	Label5->Visible = ! _value;
	eType->Visible = ! _value;
	bOK->Visible = ! _value;
	if (eType->Visible) {
		eType->SetFocus();
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
	pb1->Value = pb1->Value + 1;
	if (pb1->Value >= pb1->Max) {
		doTypeVisible(false);
	}	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	doReset();	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::bOKClick(TObject *Sender)
{
	doAnswer();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize();
	doReset();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::eTypeKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if (Key == vkReturn) {
		doAnswer();
	}	
}
//---------------------------------------------------------------------------
