//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *Button1;
	TButton *Button2;
	TLayout *Layout1;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TLabel *lRigth;
	TLabel *lWrong;
	TLabel *Label3;
	TRoundRect *RoundRect1;
	TLabel *lNum;
	TProgressBar *pb1;
	TLabel *Label5;
	TEdit *eType;
	TButton *bOK;
	TTimer *Timer1;
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall bOKClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall eTypeKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
	int cRight;
	int cWrong;
	int rNum;
	void doReset();
	void doContinue();
	void doAnswer();
    void doTypeVisible(bool _value);
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
