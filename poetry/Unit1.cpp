//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
inline int Low(const System::UnicodeString &)
{
#ifdef _DELPHI_STRING_ONE_BASED
	return 1;
#else
	return 0;
#endif
}
inline int High (const System::UnicodeString &S)
{
#ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
#else
	return S.Length() - 1;
#endif
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
    ShowMessage("Made by M. Alexeev");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    TabControl1->Scale->X = TabControl1->Scale->X + 0.1;
	TabControl1->Scale->Y = TabControl1->Scale->Y + 0.1;
	TabControl1->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
    TabControl1->Scale->X = TabControl1->Scale->X - 0.1;
	TabControl1->Scale->Y = TabControl1->Scale->Y - 0.1;
	TabControl1->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	System::UnicodeString x;
	//through line
	for (int i = 0; i < Memo1->Lines->Count; i++) {
		x = Memo1->Lines->Strings[i];
		if (i % 2 == 1) {
			for (int j = Low(x); j <= High(x); j++) {
				if (x[j] != ' ') {
					x[j] = '-';
				}
			}
		}
		Memo2->Lines->Add(x);
	}

	//first words 
	bool vFlag;
	for (int i = 0; i < Memo1->Lines->Count; i++) {
		x = Memo1->Lines->Strings[i];
		vFlag = false;
		for (int j = Low(x); j <= High(x) ; j++) {
			if (vFlag && (x[j] != ' ')) {
				x[j] = '-';
			}
			if (!vFlag && (x[j] == ' ')) {
				vFlag = true;
			}                
		}
		Memo3->Lines->Add(x);
	}
}
//---------------------------------------------------------------------------
